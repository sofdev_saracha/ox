import java.util.Scanner;

public class App {
    public static boolean Check(String[][] board, String win) {
        boolean EventA = (board[0][0] == board[0][1] && board[0][0] == board[0][2]);
        boolean EventB = (board[0][0] == board[1][1] && board[0][0] == board[2][2]);
        boolean EventC = (board[0][0] == board[1][0] && board[0][0] == board[2][0]);
        boolean EventD = (board[2][0] == board[2][1] && board[2][0] == board[2][2]);
        boolean EventE = (board[0][2] == board[1][2] && board[0][2] == board[2][2]);
        boolean EventF = (board[0][2] == board[1][1] && board[0][2] == board[2][0]);
        boolean EventG = (board[0][2] == board[1][1] && board[0][2] == board[2][1]);
        boolean EventH = (board[1][0] == board[1][1] && board[1][0] == board[1][2]);
        boolean[] Event = { EventA, EventB, EventC, EventD, EventE, EventF, EventG, EventH };
        int i;
        for (i = 0; i < Event.length; i++) {
            if (Event[i]) {
                break;
            }
        }
        if (i != Event.length) {
            if (i == 0 || i == 2) {
                win = board[0][0];
                System.out.println("Who win = " + win);
                return true;
            } else if (i == 3 || i == 4) {
                win = board[2][2];
                System.out.println("Who win = " + win);
                return true;
            } else {
                win = board[1][1];
                System.out.println("Who win = " + win);
                return true;
            }
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        String win = "-";
        String[][] board = { { "-", "-", "-" }, { "-", "-", "-" }, { "-", "-", "-" } };
        System.out.println("Hello Player");
        for (int i = 1; i <= 3; i++) {
            for (int l = 1; l <= 3; l++) {
                System.out.print(board[i - 1][l - 1]);
            }
            System.out.println();
        }
        String Turn = "O";
        int l = 0;
        for (int i = 1; i <= board.length * (board[0].length) + l; i++) {
            System.out.println("Turn " + i + " = " + Turn);
            System.out.print("Row Point = ");
            int row = sc.nextInt();
            System.out.println();
            System.out.print("Column Point = ");
            int col = sc.nextInt();
            if (row > board.length || col > board[0].length) {
                l++;
                System.out.println("Please change point");
            } else if (board[row][col] != "-") {
                l++;
                System.out.println("Please change point");
            } else {
                board[row][col] = Turn;
                if (Turn == "O") {
                    Turn = "X";
                } else {
                    Turn = "O";
                }
            }
            for (int ik = 1; ik <= 3; ik++) {
                for (int lk = 1; lk <= 3; lk++) {
                    System.out.print(board[ik - 1][lk - 1]);
                }
                System.out.println();
            }

        }
        if (!(Check(board, win))) {
            System.out.println("No one win");
        }
        sc.close();
    }
}
