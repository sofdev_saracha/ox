import java.util.Scanner;

class ProgramOX {
    private String board[][];
    private String turn = " ";
    private String win = "-";

    public void setturn(String turn) {
        this.turn = turn;
    }

    public ProgramOX(int size){
        board = new String[size][size];
        for (int i = 0 ; i < size ; i++){
            for (int l = 0 ; l < size ; l++){
                board[i][l] = "-";
            }
        }
    }

    public String[][] getBoard() {
        return board;
    }

    public void displayBoard(){
        for (int i = 0 ; i < board.length ; i++){
            for (int l = 0 ; l < board.length ; l++){
                System.out.print(board[i][l]);
            }
            System.out.println();
        }
    }
       
    public void selectPointOnBoard(int rowPoint, int colPoint){
        if (rowPoint >= board.length || colPoint >= board.length){
            System.out.println("Please change point");
        } else if (board[rowPoint][colPoint] != "-") {
            System.out.println("Please change point");
        } else {
            board[rowPoint][colPoint] = turn;
            if (turn == "O"){
                turn = "X";
            } else {
                turn = "O";
            }
        }
    }

    public boolean checkWin(){
        boolean eventA = (board[0][0] == board[0][1] && board[0][0] == board[0][2]);  
        boolean eventB = (board[0][0] == board[1][1] && board[0][0] == board[2][2]);    
        boolean eventC = (board[0][0] == board[1][0] && board[0][0] == board[2][0]);       
        boolean eventD = (board[2][0] == board[2][1] && board[2][0] == board[2][2]);
        boolean eventE = (board[0][2] == board[1][2] && board[0][2] == board[2][2]);
        boolean eventF = (board[0][2] == board[1][1] && board[0][2] == board[2][0]);
        boolean eventG = (board[0][2] == board[1][1] && board[0][2] == board[2][1]);
        boolean eventH = (board[1][0] == board[1][1] && board[1][0] == board[1][2]);
        boolean[] event = {eventA,eventB,eventC,eventD,eventE,eventF,eventG,eventH};
        int i;
        for ( i = 0 ; i < event.length ; i++){
            if (event[i]){
                break;
            }
        }
        if (i != event.length) {
            if (i == 0 || i == 2){
                win = board[0][0];
                System.out.println("Who win = " + win);
                return true;
            } else if (i == 3 || i == 4){
                win = board[2][2];
                System.out.println("Who win = " + win);
                return true;
            } else {
                win = board[1][1];
                System.out.println("Who win = " + win);
                return true;
            }
        } else {
            return false;
        }
    }

}
public class ProgramOXApp {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Size board = ");
            int sizeBoard = scanner.nextInt();
            ProgramOX programOX = new ProgramOX(sizeBoard);
            programOX.displayBoard();
            System.out.println("Firstturn = ");
            String firstturn = scanner.nextLine();
            programOX.setturn(firstturn);
            System.out.print("RowPoint = ");
            int rowPoint = scanner.nextInt();
            System.out.print("ColPoint = ");
            int colPoint = scanner.nextInt();
            programOX.selectPointOnBoard(rowPoint-1, colPoint-1);
            programOX.displayBoard();
            scanner.close();
        }
    }